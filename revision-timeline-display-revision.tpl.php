<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$vars = $variables['variables'];
?>

<?php if (isset($vars['diff_url'])): ?>
<a href="<?php print $vars['diff_url']; ?>" class="revision-timeline-action">What changed?</a>
<?php endif; ?>
<h3 class="revision-timeline-summary">
    <cite class="revision-timeline-num">Revision <?php print $vars['revision_index']; ?></cite>
    <span class="revision-timeline-label <?php if (($vars['current_vid'] == $vars['vid']) || (isset($vars['published_vid']) && $vars['published_vid'] == $vars['vid'])) { ?>label-<?php print $vars['state']; }?>"><span><em class="sr-only"><?php print ucwords($vars['state']); ?></em></span></span>
</h3>
<?php if (isset($vars['unpublish_url'])): ?>
<a href="<?php print $vars['unpublish_url'] . "?destination=node/".$vars['nid'].'/timeline'; ?>" class="revision-timeline-unpublish btn"><?php print $vars['unpublish_link_title']; ?></a>
<?php endif; ?>
<div class="revision-timeline-author"><?php print $vars['published_date'] ?>&nbsp;
    <?php print $vars['user_markup']; ?>
</div>
<ul class="revision-timeline-summary-list">
  <?php if (isset($vars['rows'])): foreach ($vars['rows'] as $index => $row): ?>
  <li class="revision-timeline-summary-note"><?php print $row ?></li>
  <?php endforeach; endif;?>
</ul>
<div class="revision-timeline-author-notes revision-timeline-comment">
    <blockquote class="revision-timeline-author-quote revision-timeline-comment-quote">
        "<?php print $vars['log_message']; ?>"
        <span class="tip"></span>
    </blockquote>
    <?php print $vars['user_quote_markup']; ?>
</div>
