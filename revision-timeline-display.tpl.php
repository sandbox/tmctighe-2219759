<?php

/**
 * @file
 * Default theme implementation for displaying a revision timeline.
 */
?>
<h2>Revisions</h2>
<ol class="revision-timeline revision-timeline-vertical" reversed>
  <?php foreach ($revisions as $index => $revision): ?>
  <li class="revision-timeline-revision">
    <?php print $revision ?>
  </li>
  <?php endforeach; ?>
</ol>

